FROM gitpod/workspace-python:latest

RUN pip install fastapi uvicorn[standard] gunicorn==20.0.4

# Instalando dependências para testes
RUN pip install -q pytest pytest-cov coverage coverage-badge

