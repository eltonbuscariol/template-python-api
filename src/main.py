from fastapi import FastAPI
from fastapi.responses import RedirectResponse

app = FastAPI(title='Template FastAPI',
              description='Template para Web API em Python')

@app.get('/', include_in_schema=False)
def base_path():
    return RedirectResponse('/docs')


@app.get('/health')
def help():
    return {
        'status': 'on'
    }
